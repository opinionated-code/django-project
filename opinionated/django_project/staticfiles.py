"""
staticfiles

Override the FileSystemFinder to run npx snowpack first, before collecting static files
"""
import logging
from django.contrib.staticfiles.finders import FileSystemFinder
from .asset_pipeline import build_assets

logger = logging.getLogger(__name__)


class OpinionatedFileSystemFinder(FileSystemFinder):
    """
    An override of the Django static files finder, that runs snowpack before collection.
    """

    def check_snowpack(self, **kwargs):
        # FIXME: Check that snowpack.config.js is valid?
        return []

    def check(self, **kwargs):
        return super().check(**kwargs) + self.check_snowpack(**kwargs)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        build_assets(watch=False)
