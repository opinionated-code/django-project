"""
Subclass the existing 'runserver' command and launch the Snowpack build watcher alongside
the django web server

This is based on the whitenoise runserver_nostatic app.

There is some unpleasant hackery here because we don't know which command class
to subclass until runtime as it depends on which INSTALLED_APPS we have, so we
have to determine this dynamically.
"""
from importlib import import_module

from django.apps import apps

from ...asset_pipeline import build_assets


def get_next_runserver_command():
    """
    Return the next highest priority "runserver" command class
    """
    for app_name in get_lower_priority_apps():
        module_path = "%s.management.commands.runserver" % app_name
        try:
            return import_module(module_path).Command
        except (ImportError, AttributeError):
            pass


def get_lower_priority_apps():
    """
    Yield all app module names below the current app in the INSTALLED_APPS list
    """
    self_app_name = ".".join(__name__.split(".")[:-3])
    reached_self = False
    for app_config in apps.get_app_configs():
        if app_config.name == self_app_name:
            reached_self = True
        elif reached_self:
            yield app_config.name
    yield "django.core"


RunserverCommand = get_next_runserver_command()


class Command(RunserverCommand):
    def __init__(self):
        from django.conf import settings

        self.default_port = str(settings.SERVER_PORT)
        super().__init__()

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            "--no-snowpack",
            dest="no_snowpack",
            action="store_true",
            default=False,
            help="Do not run snowpack build before/while running the webserver",
        )

    def run(self, **options):
        """Run the server, using the autoreloader if needed."""
        use_reloader = options["use_reloader"]

        if not options["no_snowpack"]:
            if use_reloader:
                build_assets(watch=True)
                pass
            else:
                build_assets(watch=False)
                pass

        return super().run(**options)
