"""
Subclass the existing 'rundramatiq' command and launch the Snowpack build watcher alongside
the django web server

This is based on the whitenoise runserver_nostatic app.

There is some unpleasant hackery here because we don't know which command class
to subclass until runtime as it depends on which INSTALLED_APPS we have, so we
have to determine this dynamically.
"""
from django_dramatiq.management.commands.rundramatiq import Command as RunDramatiqCommand


class Command(RunDramatiqCommand):
    def discover_tasks_modules(self):
        tasks = super().discover_tasks_modules()
        # Replace the first tasks module (which is the setup task) with one that sets up
        #  django-configurations:
        tasks[0] = "opinionated.django_project.dramatiq_setup"

        return tasks
