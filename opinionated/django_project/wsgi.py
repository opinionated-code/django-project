"""
WSGI config.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

# We use django-configurations, use that function instead of django's regular one
from configurations.wsgi import get_wsgi_application

# Must be set in env
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'opinionated.django_project.config.settings')
# os.environ.setdefault("DJANGO_CONFIGURATION", "DevConfig")

application = get_wsgi_application()
