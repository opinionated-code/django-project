"""
opinionated.django_project.forms

"""

from allauth.account.forms import (
    LoginForm,
    ResetPasswordForm,
    ResetPasswordKeyForm,
    SignupForm,
    SetPasswordForm,
    AddEmailForm,
    ChangePasswordForm,
)
from allauth.socialaccount.forms import SignupForm as SocialSignupForm, DisconnectForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML, Field, Div, Submit
from django.conf import settings
from django.urls import reverse

from .layout import FloatingLabelField


class CustomFormHelper(FormHelper):
    include_media = False


class AuthLoginForm(LoginForm):
    @property
    def helper(self):
        """Helper to build the form"""

        fh = CustomFormHelper(self)
        fh.form_action = reverse("account_login")

        layout = Layout(
            FloatingLabelField("login", autofocus=""),
            FloatingLabelField("password"),
            HTML(
                """{% if redirect_field_value %}<input type="hidden" name="{{ redirect_field_name }}" value="{{ redirect_field_value }}" />{% endif %}
"""
            ),
        )
        if settings.ACCOUNT_SESSION_REMEMBER is None:
            layout.append(Div(Field("remember"), css_class="checkbox mb-3"))

        layout.append(Submit("btn", value="Sign in", css_class="btn-lg btn-block"))
        layout.append(
            Div(
                HTML("""<a href="{% url 'account_reset_password' %}"} class="">Forgot your password?</a>"""),
                css_class="forgot-password",
            ),
        )

        fh.layout = layout

        return fh


class AuthResetPasswordForm(ResetPasswordForm):
    @property
    def helper(self):
        """Helper to build the form"""

        fh = CustomFormHelper(self)
        fh.form_action = reverse("account_reset_password")

        layout = Layout(
            FloatingLabelField("email", autofocus=""),
        )

        layout.append(Submit("btn", value="Reset my password", css_class="btn-lg btn-block"))

        fh.layout = layout

        return fh


class AuthResetPasswordKeyForm(ResetPasswordKeyForm):
    @property
    def helper(self):
        """Helper to build the form"""

        fh = CustomFormHelper(self)
        fh.form_tag = False

        layout = Layout(
            Field("password1", autofocus=""),
            Field("password2"),
        )

        layout.append(Submit("btn", value="Set Password", css_class="btn-lg btn-block"))

        fh.layout = layout

        return fh


class AuthSignupForm(SignupForm):
    @property
    def helper(self):
        """Helper to build the form"""

        fh = CustomFormHelper(self)
        fh.form_action = reverse("account_signup")

        layout = Layout(
            FloatingLabelField("email", autofocus=""),
            FloatingLabelField("password1"),
            FloatingLabelField("password2"),
            HTML(
                """{% if redirect_field_value %}<input type="hidden" name="{{ redirect_field_name }}" value="{{ redirect_field_value }}" />{% endif %}
"""
            ),
        )

        layout.append(Submit("btn", value="Sign up", css_class="btn-lg btn-block"))

        fh.layout = layout

        return fh


class AuthAddEmailForm(AddEmailForm):
    pass


class AuthChangePasswordForm(ChangePasswordForm):
    pass


class AuthSetPasswordForm(SetPasswordForm):
    pass


class AuthSocialSignupForm(SocialSignupForm):
    pass


class AuthDisconnectForm(DisconnectForm):
    pass
