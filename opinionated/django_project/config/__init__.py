"""
opinionated.django_project.config

BaseConfiguration, for managing config mixins
"""
from pathlib import Path
from typing import Union, List

from configurations import Configuration
from configurations.values import BooleanValue, Value, SecretValue, ListValue, IntegerValue

from .mixins import (
    AllauthConfigMixin,
    CrispyFormsConfigMixin,
    DjangoAnymailConfigMixin,
    LoggingConfigMixin,
    WhitenoiseConfigMixin,
    DatabaseConfigMixin,
    DramatiqConfigMixin,
)


# Set up a BaseConfiguration that we can use in our project, with a minimal set of mixins
# - allauth      (for authentication)
# - crispy forms (used in allauth)
# - anymail      (used for emailing in allauth)
# - dramatiq     (used to do async email deliveries)
# - database     (sqlite, or others)
# - whitenoise   (web serving)
# - logging      (for logging everything)
# - configuration (the django-configurations base class)
class BaseConfiguration(
    CrispyFormsConfigMixin,
    AllauthConfigMixin,
    DramatiqConfigMixin,
    DjangoAnymailConfigMixin,
    DatabaseConfigMixin,
    WhitenoiseConfigMixin,
    LoggingConfigMixin,
    Configuration,
):
    """
    Common configuration parameters for all projects.
    """

    SERVER_PORT = IntegerValue(8000)

    DJANGO_APPS = [
        # Add this at the top as we want it to override runserver and allauth templates etc
        "opinionated.django_project",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        "django.contrib.humanize",  # Handy template tags
        "django.contrib.admin",
    ]
    THIRD_PARTY_APPS: List[str] = []
    DJANGO_MIDDLEWARE = [
        "django.middleware.security.SecurityMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
    ]
    THIRD_PARTY_MIDDLEWARE: List[str] = []

    @property
    def INSTALLED_APPS(self) -> List[str]:
        """Combine DJANGO_APPS, THIRD_PARTY_APPS, and USER_APPS"""

        return self.DJANGO_APPS + self.THIRD_PARTY_APPS + self.USER_APPS

    @property
    def MIDDLEWARE(self) -> List[str]:
        """Combine DJANGO_MIDDLEWARE, THIRD_PARTY_MIDDLEWARE, USER_MIDDLEWARE"""

        return self.DJANGO_MIDDLEWARE + self.THIRD_PARTY_MIDDLEWARE + self.USER_MIDDLEWARE

    # Prefer argon2 - really no reason we would use anything else...but all three of these are "good enough"
    PASSWORD_HASHERS = [
        "django.contrib.auth.hashers.Argon2PasswordHasher",
        "django.contrib.auth.hashers.PBKDF2PasswordHasher",
        "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
    ]
    TEMPLATE_DEBUG = BooleanValue(False)
    DEBUG = BooleanValue(False)

    @property
    def TEMPLATES(self):
        """Return template configuration dynamically based on DEBUG etc"""

        # if in debug mode, skip the caching
        if self.DEBUG:
            loaders = [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ]
        else:
            loaders = [
                (
                    "django.template.loaders.cached.Loader",
                    [
                        "django.template.loaders.filesystem.Loader",
                        "django.template.loaders.app_directories.Loader",
                    ],
                ),
            ]
        return [
            {
                "BACKEND": "django.template.backends.django.DjangoTemplates",
                # Use app dir templates mostly, but for allauth and admin overrides, use /templates
                "DIRS": [
                    self.BASE_DIR / "templates",
                ],
                "OPTIONS": {
                    "loaders": loaders,
                    "debug": self.TEMPLATE_DEBUG,
                    "context_processors": [
                        "django.template.context_processors.debug",
                        "django.template.context_processors.request",
                        "django.contrib.auth.context_processors.auth",
                        "django.contrib.messages.context_processors.messages",
                    ],
                },
            },
        ]

    # Use this to force a server name, e.g. when we're in a Docker environment
    FORCE_SERVER_NAME = Value(None)
    USE_ASGI = BooleanValue(False)

    LANGUAGE_CODE = "en-us"
    TIME_ZONE = "UTC"
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True

    STATIC_URL = "/static/"
    STATICFILES_FINDERS = [
        "opinionated.django_project.staticfiles.OpinionatedFileSystemFinder",
        "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    ]

    @property
    def STATIC_ROOT(self):
        return self.BASE_DIR / "staticfiles"

    @property
    def STATICFILES_DIRS(self):
        return [
            # Snowpack built files
            self.BASE_DIR
            / "snowpack_build"
        ]

    # NOTE: This must be specified as an env var, ALWAYS, even in test mode
    SECRET_KEY = SecretValue()
    ALLOWED_HOSTS = ListValue()

    # ----- Can be set, but not essential/necessary -----

    @property
    def WSGI_APPLICATION(self):
        if self.USE_ASGI:
            return "opinionated.django_project.asgi.application"
        else:
            return "opinionated.django_project.wsgi.application"

    USER_APPS: List[str] = []
    USER_MIDDLEWARE: List[str] = []

    # -----  The following must be set in a project config -----

    @property
    def BASE_DIR(self) -> Union[str, Path]:
        """Must be set in the project"""

        raise NotImplementedError

    @property
    def APP_DIR(self) -> Union[str, Path]:
        """Must be set in the project"""

        raise NotImplementedError

    @property
    def ROOT_URLCONF(self):
        raise NotImplementedError
