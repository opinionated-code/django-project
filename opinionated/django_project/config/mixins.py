"""
mixins
"""
import logging
import string
from datetime import datetime
from functools import cached_property
import random
from typing import Dict, Optional, Union, Type
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from configurations.values import (
    IntegerValue,
    BooleanValue,
    BackendsValue,
    Value,
    CacheURLValue,
    DatabaseURLValue,
    EmailValue,
    DictValue,
    URLValue,
    FloatValue,
)

from opinionated.django_project.config.utils import AnyURLValue, LogLevelValue

try:
    from google.oauth2 import service_account
except ImportError:
    service_account = None


class GenerateTestSecretKeyMixin(object):
    def generate_test_secret_key(self):
        return "".join(random.choice(string.ascii_letters + string.digits) for i in range(15))


class AllauthConfigMixin(object):
    """
    Mixin to provide django-allauth configuration.
    """

    # URLs:
    #   path('accounts/', include('allauth.urls')),
    ALLAUTH_SOCIALACCOUNT_PROVIDERS = BackendsValue([])
    ALLAUTH_USE_EMAIL_AS_USERNAME = True
    ALLAUTH_ALLOW_SIGNUP = BooleanValue(True)
    # Always require email. Unless you're building a service that provides extreme anonymity or is an actual
    # email service, you probably want to rely on email as the username, rather than an arbitrary username.
    ACCOUNT_EMAIL_REQUIRED = True
    # Usability: for simplicity of experience, unless there is no email, always use email as username.
    ACCOUNT_AUTHENTICATION_METHOD = "email"
    ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = IntegerValue(3)
    # For usability, "optional" is good. Let people log in, but probably limit functionality until they verify.
    ACCOUNT_EMAIL_VERIFICATION = "optional"
    # use https unless there is a sane reason not to
    ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"
    ACCOUNT_MAX_EMAIL_ADDRESSES = IntegerValue(None)
    # Undesirable - has performance penalty to set to True, and emails are not case sensitive anyway
    ACCOUNT_PRESERVE_USERNAME_CASING = False
    # Irrelevant for socialaccount logins - for others, maybe?
    ACCOUNT_SESSION_REMEMBER = None
    # Personal preference, minimum of 2. Practically, if using email as username, it'll be much more than that.
    ACCOUNT_USERNAME_MIN_LENGTH = 2
    # Don't require username (we populate it from email in the adapter anyway)
    ACCOUNT_USERNAME_REQUIRED = False
    ACCOUNT_ADAPTER = "opinionated.django_project.authentication.AccountAdapter"
    SOCIALACCOUNT_ADAPTER = "opinionated.django_project.authentication.SocialAccountAdapter"

    @classmethod
    def setup(cls):
        super().setup()
        # Make sure django.contrib.sites is in DJANGO_APPS
        # We do this in setup() because we can't be sure it has been added in pre_setup()
        for required_app in {"django.contrib.auth", "django.contrib.messages", "django.contrib.sites"}:
            if required_app not in cls.DJANGO_APPS:
                cls.DJANGO_APPS.append(required_app)
        cls.THIRD_PARTY_APPS += [
            "allauth",
            "allauth.account",
            "allauth.socialaccount",
            *cls.ALLAUTH_SOCIALACCOUNT_PROVIDERS,
        ]

    SOCIALACCOUNT_PROVIDERS = {}
    AUTHENTICATION_BACKENDS = [
        # Internal accounts - needs to be here in case allauth is borken
        "django.contrib.auth.backends.ModelBackend",
        # Use allauth as primary
        "allauth.account.auth_backends.AuthenticationBackend",
    ]
    AUTH_PASSWORD_VALIDATORS = [
        {
            "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
        },
    ]

    ACCOUNT_FORMS = {
        "login": "opinionated.django_project.forms.AuthLoginForm",
        "reset_password": "opinionated.django_project.forms.AuthResetPasswordForm",
        "reset_password_from_key": "opinionated.django_project.forms.AuthResetPasswordKeyForm",
        "signup": "opinionated.django_project.forms.AuthSignupForm",
        "add_email": "opinionated.django_project.forms.AuthAddEmailForm",
        "change_password": "opinionated.django_project.forms.AuthChangePasswordForm",
        "set_password": "opinionated.django_project.forms.AuthSetPasswordForm",
    }
    SOCIALACCOUNT_FORMS = {
        "signup": "opinionated.django_project.forms.AuthSocialSignupForm",
        "disconnect": "opinionated.django_project.forms.AuthDisconnectForm",
    }

    # Possibly shouldn't set this - ideally it gets resolved from the hostname of the web request
    SITE_ID = IntegerValue(1)


class DjangoExtensionsConfigMixin(object):
    """
    Mixin to add django-extensions support.
    We only use this in Dev, because some of the tools are best kept away from prod environments.
    """

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("django_extensions")


class DramatiqConfigMixin(object):
    """
    Mixin to provide django-dramatiq configuration.
    """

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        # Insert at the top - makes sure we're instantiated before cadence. Shouldn't matter but better safe.
        cls.THIRD_PARTY_APPS.insert(0, "django_dramatiq")

    DRAMATIQ_BROKER_BACKEND = Value("dramatiq.brokers.redis.RedisBroker")
    DRAMATIQ_BROKER_URL = AnyURLValue()
    DRAMATIQ_TASKS_DATABASE = "default"
    DRAMATIQ_RESULTS_BACKEND = Value(None)
    DRAMATIQ_RESULTS_URL = AnyURLValue()

    DRAMATIQ_IGNORED_MODULES = (
        # dotted import path for modules
    )

    @property
    def DRAMATIQ_BROKER(self):
        res = {
            "BROKER": self.DRAMATIQ_BROKER_BACKEND,
            "OPTIONS": {},
            "MIDDLEWARE": [
                "dramatiq.middleware.Prometheus",
                "dramatiq.middleware.AgeLimit",
                "dramatiq.middleware.TimeLimit",
                "dramatiq.middleware.Callbacks",
                "dramatiq.middleware.Retries",
                "dramatiq.middleware.ShutdownNotifications",
                "django_dramatiq.middleware.DbConnectionsMiddleware",
                "django_dramatiq.middleware.AdminMiddleware",
            ],
        }
        if self.DRAMATIQ_BROKER_URL is not None and len(self.DRAMATIQ_BROKER_URL) > 0:
            res["OPTIONS"]["url"] = self.DRAMATIQ_BROKER_URL
        return res

    @property
    def DRAMATIQ_RESULT_BACKEND(self):
        if self.DRAMATIQ_RESULTS_BACKEND is not None:
            return {
                "BACKEND": self.DRAMATIQ_RESULTS_BACKEND,
                "BACKEND_OPTIONS": {
                    "url": self.DRAMATIQ_RESULTS_URL,
                },
                "MIDDLEWARE_OPTIONS": {"result_ttl": 60000},
            }
        else:
            return None


class DefaultCacheConfigMixin(object):
    """
    Mixin to provide basic in-memory cache configuration.
    """

    CACHES = CacheURLValue("locmem://default")


class RedisCacheConfigMixin(object):
    """
    Mixin to provide django-redis cache configuration.
    """

    CACHE_REDIS_URL = AnyURLValue("redis://127.0.0.1:6379/0")
    CACHE_REDIS_PASSWORD = Value(None)

    @property
    def CACHES(self):
        return {
            "default": {
                "BACKEND": "django_redis.cache.RedisCache",
                # Note: a list can be specified for LOCATION to set up replicas.
                "LOCATION": self.CACHE_REDIS_URL,
                "OPTIONS": {
                    # With multiple location, using ShardClient instead of Default provides sharding (experimental).
                    "CLIENT_CLASS": "django_redis.client.DefaultClient",
                    "PASSWORD": self.CACHE_REDIS_PASSWORD,
                    # requires hiredis package. Significantly faster. Not sure about trade-offs?
                    # "PARSER_CLASS": "redis.connection.HiredisParser"
                },
            }
        }

    # Use for session cache
    SESSION_ENGINE = "django.contrib.sessions.backends.cache"
    SESSION_CACHE_ALIAS = "default"


class CrispyFormsConfigMixin(object):
    """
    Mixin to provide djstripe configuration.
    """

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("crispy_forms")

    CRISPY_TEMPLATE_PACK = "bootstrap4"

    # Template packs allowed, overridden to include opinionated-forms
    # CRISPY_ALLOWED_TEMPLATE_PACKS = ["opinionated-forms", ]
    # CRISPY_TEMPLATE_PACK = "opinionated-forms"


class DatabaseConfigMixin(object):
    """
    Mixin to provide sqlite configuration.
    """

    DATABASES = DatabaseURLValue(f"sqlite:///db.sqlite", environ_prefix="DJANGO")

    @classmethod
    def post_setup(cls):
        """Check db is ready"""

        # FIXME: Add a script to check the db - if sqlite, go ahead, if postgres, check if it is ready and wait...
        pass


def show_toolbar_callback(req) -> bool:
    """Return whether debug toolbar is allowed for this request"""

    # FIXME: Come up with a way to check if we're in docker ?
    return True


class DebugToolbarConfigMixin(object):
    """
    Mixin to provide debug properties, debug toolbar, etc.
    We keep logging simple - stdout streaming logs only, no files or syslog.
    """

    # URLs:
    #   path('__debug__/', include(debug_toolbar.urls)),

    @classmethod
    def pre_setup(cls):
        """Add django-toolbar to the apps list, if DEBUG_TOOLBAR is True"""

        super().pre_setup()

        cls.THIRD_PARTY_APPS.append("debug_toolbar")
        # Add middleware to top of third-party. It should be ideally as early as possible in the list.
        cls.THIRD_PARTY_MIDDLEWARE.insert(0, "debug_toolbar.middleware.DebugToolbarMiddleware")

    DEBUG_TOOLBAR = BooleanValue(True)
    DEBUG_TOOLBAR_CONFIG = {
        "SHOW_COLLAPSED": True,
        "SHOW_TOOLBAR_CALLBACK": "opinionated.django_project.config.mixins.show_toolbar_callback",
    }
    # FIXME: Consider adding https://github.com/node13h/django-debug-toolbar-template-profiler to panels


class StripeConfigMixin(object):
    """
    Mixin to provide djstripe configuration.
    """

    # URLs
    #  path("stripe/", include("djstripe.urls", namespace="djstripe")),

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("djstripe")

    STRIPE_LIVE_SECRET_KEY = Value("")
    STRIPE_TEST_SECRET_KEY = Value("")
    STRIPE_LIVE_MODE = BooleanValue(False)
    DJSTRIPE_WEBHOOK_SECRET = Value("")
    DJSTRIPE_USE_NATIVE_JSONFIELD = True
    DJSTRIPE_FOREIGN_KEY_TO_FIELD = "id"


class GrapheneConfigMixin(object):
    """
    Mixin to provide graphene-django configuration.
    """

    # URLs
    #  path(r"graphql/", GraphQLView.as_view(graphiql=True))

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("graphene_django")

    @property
    def GRAPHENE_SCHEMA(self):
        raise NotImplementedError

    @property
    def GRAPHENE(self):
        return {"SCHEMA": self.GRAPHENE_SCHEMA}


class LoggingConfigMixin(object):
    """
    Mixin to provide logging properties.
    We keep logging simple - stdout streaming logs only, no files or syslog.
    """

    LOG_LEVEL = LogLevelValue(None, environ_name="LOGGING_LEVEL")

    @property
    def LOGGING(self):
        """Logging config, using DJANGO_LOGGING_LEVEL environment variable, with some overrides"""

        if self.LOG_LEVEL is None:
            log_level = "DEBUG" if self.DEBUG else "WARNING"
        else:
            log_level = self.LOG_LEVEL

        return {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "django_log_fmt": {
                    "format": "%(levelname)s %(asctime)s %(name)s %(module)s %(process)d %(thread)d %(message)s"
                },
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "django_log_fmt",
                },
            },
            "root": {
                "handlers": ["console"],
                "level": log_level,
            },
            "loggers": {
                "django.utils.autoreload": {
                    # Max INFO for autoreload, otherwise we get meaningless spam while in development
                    "level": "INFO",
                },
                "django.db.backends": {
                    # Max INFO for db, otherwise we get meaningless spam
                    "level": "INFO",
                },
                "django.template": {
                    # No need for debug logging on templates unless we're investigating something unusual
                    "level": "INFO",
                },
            },
        }


class DjangoAnymailConfigMixin(object):
    """
    Mixin to provide django-anymail configuration.
    """

    # URLs
    #   re_path(r"webhooks/anymail/", include("anymail.urls")),
    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("anymail")

    EMAIL_BACKEND = Value("anymail.backends.mailgun.EmailBackend")

    DEFAULT_FROM_EMAIL = EmailValue("webmaster@localhost")
    SERVER_EMAIL = EmailValue("root@localhost")

    ANYMAIL_MAILGUN_API_KEY = Value()
    ANYMAIL_SEND_DEFAULTS = DictValue({})
    ANYMAIL_WEBHOOK_SECRET = Value()
    ANYMAIL_MAILGUN_SENDER_DOMAIN = Value()
    ANYMAIL_MAILGUN_WEBHOOK_SIGNING_KEY = Value()
    ANYMAIL_MAILGUN_API_URL = AnyURLValue("https://api.mailgun.net/v3")

    ANYMAIL_MAILJET_API_KEY = Value()
    ANYMAIL_MAILJET_SECRET_KEY = Value()
    ANYMAIL_MANDRILL_API_KEY = Value()
    ANYMAIL_MANDRILL_WEBHOOK_KEY = Value()
    ANYMAIL_MANDRILL_WEBHOOK_URL = AnyURLValue()

    ANYMAIL_POSTMARK_SERVER_TOKEN = Value()

    ANYMAIL_SENDGRID_API_KEY = Value()

    ANYMAIL_SENDINBLUE_API_KEY = Value()

    ANYMAIL_SPARKPOST_API_KEY = Value()
    ANYMAIL_SPARKPOST_SUBACCOUNT = IntegerValue()


class DjangoRestFrameworkConfigMixin(object):
    """
    Mixin to provide django-rest-framework configuration.
    """

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("rest_framework")


class DjangoCadenceConfigMixin(object):
    """
    Mixin to provide django-cadence configuration.
    """

    @classmethod
    def pre_setup(cls):
        super().pre_setup()
        cls.THIRD_PARTY_APPS.append("django_cadence")

    # Currently no other configuration required.


class SentryConfigMixin(object):
    """
    Mixin to provide Sentry configuration. Not needed for dev, just for prod and staging.
    """

    # FIXME: We should add a templatetag for putting sentry in frontend (and passing DSN through)

    SENTRY_DSN = AnyURLValue(None, optional=True)
    SENTRY_JS_DSN = AnyURLValue(None, optional=True)
    SENTRY_ENVIRONMENT = Value("staging")
    SENTRY_SAMPLE_RATE = FloatValue(1.0)
    SENTRY_SEND_PII = BooleanValue(True)
    SENTRY_INTEGRATIONS = BackendsValue([])

    # We use cached property to avoid reading the file multiple times
    @cached_property
    def SENTRY_RELEASE_INFO(self) -> Dict[str, Optional[Union[str, datetime]]]:
        """Get some Sentry release data"""
        res = {"release": None, "release_full": None, "version": None, "date": None}
        # In the dockerfile, we write the release to the image in '.release'. Try to read it from file
        sentry_release_file = self.BASE_DIR / ".release"
        if sentry_release_file.exists():
            txt = sentry_release_file.read_text().strip()
            res["date"] = datetime.fromtimestamp(sentry_release_file.stat().st_mtime)
            if len(txt) > 7:
                # If very long it is likely a full git SHA; make it shorter
                res["release_full"] = txt
                res["release"] = txt[-7:]
            elif len(txt) > 0:
                res["release_full"] = txt
                res["release"] = txt
        # FIXME: use poetry-version to extract version from pyproject.toml
        return res

    def __init__(self):
        super().__init__()
        if self.SENTRY_DSN in {None, ""}:
            return
        sentry_release_info = self.SENTRY_RELEASE_INFO
        sentry_integrations = [LoggingIntegration(event_level=logging.ERROR), DjangoIntegration(transaction_style="function_name"),] + self.SENTRY_INTEGRATIONS
        DramatiqIntegration: "Optional[Type[DramatiqIntegration]]"
        try:
            from sentry_dramatiq import DramatiqIntegration
            sentry_integrations.append(DramatiqIntegration())
        except ImportError:
            DramatiqIntegration = None
        sentry_sdk.init(
            dsn=self.SENTRY_DSN,
            integrations=sentry_integrations,
            release=sentry_release_info["release"],
            # adjust as needed...
            traces_sample_rate=self.SENTRY_SAMPLE_RATE,
            send_default_pii=self.SENTRY_SEND_PII,
        )


class DjangoStorageConfigMixin(object):
    """
    Mixin to provide django-storage configuration.
    """

    # DEFAULT_FILE_STORAGE = values.Value("storages.backends.gcloud.GoogleCloudStorage")
    DEFAULT_FILE_STORAGE = Value("storages.backends.s3boto3.S3Boto3Storage")
    # Incompatible with using Whitenoise compressed storage; disable that if using this
    # STATICFILES_STORAGE = values.Value("storages.backend.s3boto.S3Boto3Storage")

    # S3 settings
    AWS_ACCESS_KEY_ID = Value()
    AWS_SECRET_ACCESS_KEY = Value()
    AWS_STORAGE_BUCKET_NAME = Value()
    AWS_S3_OBJECT_PARAMETERS = DictValue({})
    AWS_DEFAULT_ACL = Value(None)
    AWS_IS_GZIPPED = BooleanValue(False)
    AWS_S3_REGION_NAME = Value()
    AWS_S3_ENDPOINT_URL = AnyURLValue()
    AWS_S3_USE_SSL = BooleanValue(True)
    AWS_LOCATION = Value("")

    # Gcloud settings
    GS_BUCKET_NAME = Value()
    GS_PROJECT_ID = Value()

    GS_CREDENTIALS_DATA = DictValue()
    GS_CREDENTIALS_FILE = Value()

    @cached_property
    def GS_CREDENTIALS(self):
        if service_account is None:
            raise NotImplementedError("No support for Google Cloud storage - google oauth package not installed")
        if self.GS_CREDENTIALS_DATA is not None and len(self.GS_CREDENTIALS_DATA) > 0:
            return service_account.Credentials.from_service_account_info(self.GS_CREDENTIALS_DATA)
        elif self.GS_CREDENTIALS_FILE is not None:
            return service_account.Credentials.from_service_account_file(self.GS_CREDENTIALS_FILE)
        else:
            return None

    GS_DEFAULT_ACL = Value(None)
    # Multiple of 256k
    GS_BLOB_CHUNK_SIZE = IntegerValue(None)
    GS_LOCATION = Value("")


class WhitenoiseConfigMixin(object):
    """
    Mixin to provide whitenoise settings.
    """

    # For CDNs, see http://whitenoise.evans.io/en/stable/django.html#use-a-content-delivery-network
    STATIC_HOST = AnyURLValue(None)
    WHITENOISE_KEEP_ONLY_HASHED_FILES = BooleanValue(False)

    # Potentially add favicon.ico, robots.txt, etc to this dir and enable:
    # WHITENOISE_ROOT = BASE_DIR / "persistent_staticfiles"

    @classmethod
    def setup(cls):
        super().setup()
        # Insert middleware as second item (right below django Security middleware that is first)
        cls.DJANGO_MIDDLEWARE.insert(1, "whitenoise.middleware.WhiteNoiseMiddleware")
        # Insert whitenoise runserver override
        cls.DJANGO_APPS.insert(0, "whitenoise.runserver_nostatic")

    @classmethod
    def post_setup(cls):
        # Make sure the static root is working.
        if not cls.STATIC_ROOT.exists():
            cls.STATIC_ROOT.mkdir(parents=True)
        super().post_setup()

    # Preferred (for production, at least):
    STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
