"""
utils.py

opinionated.django_project.config.utils

"""
import logging
import re

from configurations.values import URLValue, ValidationMixin, Value
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.utils.regex_helper import _lazy_re_compile

LOG_LEVELS = {
    "warning": logging.WARNING,
    "error": logging.ERROR,
    "info": logging.INFO,
    "debug": logging.DEBUG,
    "critical": logging.CRITICAL,
}


class AnyURLValidator(URLValidator):
    """Slightly less stupid url validator that should work with redis"""

    # Django's URLValidator does not consider an unqualified hostname (e.g. "broker") valid.
    # But docker service addresses are hostnames.
    # So we brute-force solve this by allowing just the hostname.
    host_re = '(' + URLValidator.hostname_re + '|' + URLValidator.hostname_re + URLValidator.domain_re + URLValidator.tld_re + '|localhost)'

    regex = _lazy_re_compile(
        r'^(?:[a-z0-9.+-]*)://'  # scheme is validated separately
        r'(?:[^\s:@/]+(?::[^\s:@/]*)?@)?'  # user:pass authentication
        r'(?:' + URLValidator.ipv4_re + '|' + URLValidator.ipv6_re + '|' + host_re + ')'
                                                           r'(?::\d{2,5})?'  # port
                                                           r'(?:[/?#][^\s]*)?'  # resource path
                                                           r'\Z', re.IGNORECASE)


class MyValidationMixin(ValidationMixin):
    def __init__(self, *args, optional=False, **kwargs):
        self.__optional = optional
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        try:
            if len(value) == 0 and self.__optional:
                # Optional, allowed to be blank
                return value
            self._validator(value)
        except ValidationError:
            if getattr(self, "environ_name", None) is not None:
                raise ValueError(self.message.format(f"{self.environ_name}={value}"))
            else:
                raise ValueError(self.message.format(value))
        else:
            return value


class AnyURLValue(MyValidationMixin, Value):
    def full_environ_name(self, name):
        self.environ_name = super().full_environ_name(name)
        return self.environ_name

    message = 'Cannot interpret URL value {0!r}'
    validator = AnyURLValidator(
        schemes=[
            "http",
            "https",
            "ftp",
            "ftps",
            "redis",
            "amqp",
            "ssh",
            "git",
        ]
    )


def check_allowed_log_level(value):
    if value.lower() in LOG_LEVELS.keys():
        return True
    else:
        return False


class LogLevelValue(ValidationMixin, Value):
    validator = check_allowed_log_level
