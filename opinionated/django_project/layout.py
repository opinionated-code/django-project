"""
Add some extra layout field(s) for crispy forms

"""
from crispy_forms.layout import Field


class FloatingLabelField(Field):
    template = "%s/layout/floating_label_field.html"
