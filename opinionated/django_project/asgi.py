"""
ASGI config.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import django
from django.core.handlers.asgi import ASGIHandler
from configurations import importer


# We use django-configurations, use our own function instead of django's regular one
def get_asgi_application(set_prefix=False):
    importer.install()
    django.setup()
    return ASGIHandler()


# Must be set in the env
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'opinionated.django_project.config.settings')
# os.environ.setdefault("DJANGO_CONFIGURATION", "DevConfig")
application = get_asgi_application()
