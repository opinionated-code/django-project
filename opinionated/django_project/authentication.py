from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter


class AccountAdapter(DefaultAccountAdapter):
    """Overrde this in the proejct for more granular control"""

    def is_open_for_signup(self, request):
        """
        Return based on setting
        """
        from django.conf import settings

        # FIXME: Pass through to the current site config ?
        return settings.ALLAUTH_ALLOW_SIGNUP

    def populate_username(self, request, user):
        """
        If setting is true, populate the email address as the username
        """

        from django.conf import settings

        if settings.ALLAUTH_USE_EMAIL_AS_USERNAME and (
                (user.username is None or len(user.username) == 0) and user.email is not None
        ):
            user.username = user.email

        return super().populate_username(request, user)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    """Override this in the project for more granular control"""

    # Currently nothing we need to do here
    pass
