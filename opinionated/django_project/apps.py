"""
apps.py

Initialize the app

"""
from django.apps import AppConfig


class DjangoProjectConfig(AppConfig):
    """Configuration class for the djangoserver application and project."""

    name: str = "opinionated.django_project"
    verbose_name: str = "Opinionated Django Project"
    done: bool = False

    has_assets: bool = True
    # No asset_path, use the default (base module path with "assets" folder)
    # asset_path: str = ""

    def ready(self):
        """"Called when all app registry is populated but before models are loaded etc"""

        if not self.done:
            self.done = True

        # Do any special one-time setup we need to here.
