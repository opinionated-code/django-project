/* Load bootstrap via snowpack */
import $ from "jquery"
import 'bootstrap'
// We don't use this here but import it so it is available for html and css usage
import 'bootstrap-icons/bootstrap-icons.svg'

/* Our JS code */

$('[data-toggle="offcanvas"]').on('click', () => $('.offcanvas-collapse').toggleClass('open'))
