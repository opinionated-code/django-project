# Changelog

All notable changes to this project will be documented in this file.

When updating, please note the format complies with the style described at [Keep a Changelog].
Versions in this project strictly adhere to [Semantic Versioning].

## [Unreleased]

## [0.1.0] - 2020-06-11
### Added
- Github project created by [@LucidDan] and first version published to pypi.



[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

[Unreleased]: https://github.com/LucidDan/opinionated-django-project/compare/0.1.1...HEAD
[0.1.0]: https://github.com/LucidDan/opinionated-django-project/releases/tag/0.1.0
