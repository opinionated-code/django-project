"""
test_collectstatic

Test the collectstatic command.
"""

from io import StringIO, SEEK_SET

from django.core.management import call_command


def test_run_collectstatic_command(settings, mocker):
    out = StringIO()

    call_command("collectstatic", clear=True, verbosity=1, interactive=False, stdout=out, stderr=out)

    out.seek(SEEK_SET)
    assert " static files copied " in out.read()
