#  Copyright (c) 2020 Daniel Sloan, Lucid Horizons Pty Ltd
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at https://mozilla.org/MPL/2.0/.
#


def test_no_smoke(settings, mocker):
    """
    A simple smoke test, that creates a test project based on opinionated django project,
    and attempts to verify that it starts normally and the web server comes up and is
    accessible.
    """

    assert True
