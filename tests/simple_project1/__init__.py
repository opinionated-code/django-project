"""
simple_project1

This is a test project that includes the django_project app, to allow us to easily
test the app functionality in a complete django environment.

Some of the other unit tests don't require this, because they can run in an isolated
context, but certain things (such as management commands) require a more complete
django environment to test properly.
"""
