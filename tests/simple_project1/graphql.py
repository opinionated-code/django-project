"""
tests.simple_project1.graphql

Simple Graphene schema
"""

import graphene


class Query(graphene.ObjectType):
    hello = graphene.String(default_value="Hi!")


schema = graphene.Schema(query=Query)
