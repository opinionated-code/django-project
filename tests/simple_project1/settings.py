"""
sample_project1.settings

This module contains django-configurations style settings classes that inherit from
django_project
"""
from pathlib import Path

from opinionated.django_project.config import BaseConfiguration
from opinionated.django_project.config.mixins import (
    GenerateTestSecretKeyMixin,
    DefaultCacheConfigMixin,
    RedisCacheConfigMixin,
    DebugToolbarConfigMixin,
    GrapheneConfigMixin,
    DjangoRestFrameworkConfigMixin,
    DjangoCadenceConfigMixin,
    SentryConfigMixin,
    DjangoStorageConfigMixin,
    DjangoExtensionsConfigMixin,
)


# Valid django-configurations that can be used:
__all__ = ["SimpleProjectConfig",]


class SimpleProjectConfig(
    GenerateTestSecretKeyMixin,
    RedisCacheConfigMixin,
    DefaultCacheConfigMixin,
    DjangoExtensionsConfigMixin,
    DebugToolbarConfigMixin,
    SentryConfigMixin,
    DjangoRestFrameworkConfigMixin,
    GrapheneConfigMixin,
    DjangoStorageConfigMixin,
    DjangoCadenceConfigMixin,
    BaseConfiguration,
):
    """Common configuration parameters for all deployments in all my projects"""

    # Required - for this test project we put BASE_DIR as the same dir as the project.
    BASE_DIR = Path(__file__).resolve().parents[0]
    APP_DIR = Path(__file__).resolve().parents[0]

    ROOT_URLCONF = "tests.simple_project1.urls"

    @property
    def SECRET_KEY(self):
        return self.generate_test_secret_key()

    GRAPHENE_SCHEMA = "tests.simple_project1.graphql.schema"
    USER_APPS = ["tests.simple_project1.apps.SampleProjectAppConfig"]

    #ALLAUTH_SOCIALACCOUNT_PROVIDERS = ["google", whatever]
    #ACCOUNT_ADAPTER = "tests.simple_project1.allauth_adapters.AccountAdapter"
    #SOCIALACCOUNT_ADAPTER = "tests.simple_project1..allauth_adapters.SocialAccountAdapter"
