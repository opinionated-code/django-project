"""
simple_project1.apps

Initialize the Sample Project

"""
from django.apps import AppConfig


class SampleProjectAppConfig(AppConfig):
    """Configuration class for the sample project."""

    name: str = "tests.simple_project1"
    verbose_name: str = "My Sample Project"

    # We have assets
    has_assets: bool = True
